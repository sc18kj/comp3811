#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QMessageBox>

const unsigned int N_X_IMAGE = 128;
const unsigned int N_Y_IMAGE = 128;
const unsigned int N_COLOR   = 3;

class PyramidWidget: public QGLWidget
	{ // 

	Q_OBJECT

	public:
	PyramidWidget(QWidget *parent);
	~PyramidWidget();

	public slots:
        // called by the timer in the main window
	void updateAngle();

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:

	void pyramid();
	void fillimage();

	int _w;
	int _h;

	double _angle;

     
	GLubyte image[N_X_IMAGE][N_Y_IMAGE][N_COLOR];
	GLubyte check[512];

	QImage* p_qimage;


	}; // class GLPolygonWidget
	
#endif
