#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QBoxLayout>
#include "PyramidWidget.h"

class MainWindow: public QWidget
	{ 
	public:
       
	
	// constructor / destructor
	MainWindow(QWidget *parent);
	~MainWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;

	// beneath that, the main widget
	PyramidWidget *cubeWidget;
	// and a slider for the number of vertices
	QSlider *nVerticesSlider;

	// a timer
	QTimer *ptimer;

	// resets all the interface elements
	void ResetInterface();
	}; 
	
#endif
