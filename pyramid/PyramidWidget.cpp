#include <GL/glu.h>
#include <GL/glut.h>
#include <QGLWidget>
#include <QDebug>
#include <cmath>
#include <fstream>
#include "PyramidWidget.h"


// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8 
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};

void PyramidWidget::fillimage() {

    for(unsigned int i_x_pix = 0; i_x_pix < N_X_IMAGE; i_x_pix++)
      for(unsigned int i_y_pix = 0; i_y_pix < N_Y_IMAGE; i_y_pix++){
      QRgb colval = p_qimage->pixel(i_x_pix,i_y_pix);
      image[N_Y_IMAGE - 1 - i_y_pix][i_x_pix][0] = qRed(colval);
      image[N_Y_IMAGE - 1 - i_y_pix][i_x_pix][1] = qGreen(colval);
      image[N_Y_IMAGE - 1 - i_y_pix][i_x_pix][2] = qBlue(colval);
    }
}


// constructor
PyramidWidget::PyramidWidget(QWidget *parent):
  QGLWidget(parent),
  _w(0),
  _h(0),
  _angle(0.0)
	{ // constructor

	p_qimage = new QImage("Textures/Marc_Dekamps.ppm"); //read image object useing this.... use relitve path not abaolute
    fillimage();

      GLubyte  wb[2] = {0x00, 0xff};
	  for(int i = 0; i < 64; i++)
	    for(int j = 0; j < 8; j++)
	    check[i*8+j]=wb[(i/8+j)%2]; //gives an index and keeps track of index is odd or even 
	 
	} // constructor


PyramidWidget::~PyramidWidget()
{
  delete p_qimage;
}

// called when OpenGL context is set up
void PyramidWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
 
	} // initializeGL()


// called every time the widget is resized
void PyramidWidget::resizeGL(int w, int h)
	{ // resizeGL()
	  _w = w;
	  _h = h;

	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);

	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);

	} // resizeGL()

void PyramidWidget::updateAngle(){
  _angle += 1.0;
  this->repaint();
}	

void PyramidWidget::pyramid(){
  GLfloat normals[][3] = { { (GLfloat)(1./sqrt(3.)), (GLfloat)(1./sqrt(3.)), (GLfloat)(1./sqrt(3.))},
			   { (GLfloat)(-1./sqrt(3.)), (GLfloat)(1./sqrt(3.)), (GLfloat)(1./sqrt(3.))},
			   { (GLfloat)(-1./sqrt(3.)), (GLfloat)(1./sqrt(3.)), (GLfloat)(-1./sqrt(3.))},
			   { (GLfloat)(1./sqrt(3.)), (GLfloat)(1./sqrt(3.)), (GLfloat)(-1./sqrt(3.))} };
	// create one face with brassMaterials
	materialStruct* p_front = &whiteShinyMaterials;
	
	glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


	glNormal3fv(normals[0]);
	glBegin(GL_POLYGON);
	glVertex3f(1.,0.,0.);
	glVertex3f(0.,0.,1.);
	glVertex3f(0.,1.,0.);
	glEnd();

	// create one face with brassMaterials
	//        p_front = &brassMaterials;
	
	glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


	glNormal3fv(normals[1]);
	glBegin(GL_POLYGON);
	glVertex3f(-1.,0.,0.);
	glVertex3f(0.,0.,1.);
	glVertex3f(0.,1.,0.);
	glEnd();

	glNormal3fv(normals[2]);
	glBegin(GL_POLYGON);
	glVertex3f(-1.,0.,0.);
	glVertex3f(0.,0.,-1.);
	glVertex3f(0.,1.,0.);
	glEnd();

	glNormal3fv(normals[3]);
	glBegin(GL_POLYGON);
	glVertex3f(1.,0.,0.);
	glVertex3f(0.,0.,-1.);
	glVertex3f(0.,1.,0.);
	glEnd();

}

// called every time the widget needs painting
void PyramidWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	GLfloat light_pos[] = {0. ,  4.,  4., 0.};
	GLfloat diffuse0[]  = {0.0, 0.9, 0., 1.};
	GLfloat ambient0[]  = {0.2, 0.2, 0.2, 1.};
	GLfloat specular0[]  = {0., 0., 1.0, 1.};

	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular0);
	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 180.);

   	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glPopMatrix();

	glRotatef(_angle, 0.,2., 0.);
	glScalef(3.0,3.0,3.0);

        pyramid();
        //uncomment for flying picture
        glRasterPos4f(0.,0.,2.,1.);
        glDrawPixels(N_X_IMAGE,N_Y_IMAGE,GL_RGB, GL_UNSIGNED_BYTE, image);


	glLoadIdentity();
       	gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);



        // to draw bit maps over the current image and use viewport coordinates, do:                                                                                             
//switch to useing pixle cordinates 
        glMatrixMode(GL_PROJECTION);                                                                                                                                      
	glPushMatrix(); // push the current projection matrix
	glLoadIdentity();                           
        glOrtho(0,_w,0,_h,-1,1);                                                                                                                                                    
    

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
       	glDisable(GL_LIGHTING);
	glColor3f(1.0, 0.0, 0.0); 
       
        glRasterPos3i(10,400,1);
       
	std::string s = "Attend the Lab Sessions!!";
	for (auto i:  s)
	  {
	    char c = i;
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,c);
	  }	
       	
	
	glColor3f(0.0, 0.0, 1.); 
	       
        // texture example
	glRasterPos3i(0,0,1);
	glBitmap(64, 64, 0., 0., 60., 60., check); //reset the thing
	glBitmap(64, 64, 0., 0., 60., 60., check);
	glBitmap(64, 64, 0., 0.,  0., 60., check);
	glBitmap(64, 64, 0., 0.,  0., 60., check);
	glBitmap(64, 64, 0., 0., 60., 60., check);
	glBitmap(64, 64, 0., 0., 60., 60., check);
	glBitmap(64, 64, 0., 0., 60., 0., check);
	glBitmap(64, 64, 0., 0., 60., 0., check);
       						  	
	// restore projection matrix and perspective matrix
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glEnable(GL_LIGHTING);

	

	
	// flush to screen
	glFlush();	

	} // paintGL()
