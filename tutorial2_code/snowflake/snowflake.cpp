
#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QDesktopWidget>
#include <QKeyEvent>
#include <iostream>

class MyWidget : public QWidget {
public:

  // mouse tracking must be enabled
  void EnableMouse();
  MyWidget(QWidget *parent = 0);

protected:

  virtual void paintEvent(QPaintEvent*);
  virtual void mouseMoveEvent(QMouseEvent*);
  void keyPressEvent(QKeyEvent *);
  void keyReleaseEvent(QKeyEvent *);


private:

};

MyWidget::MyWidget(QWidget *parent) :
    QWidget(parent)
{

 
}
 
void MyWidget::keyPressEvent(QKeyEvent *event)
{
  if(event->key()==Qt::Key_Escape){
      QMessageBox::information(this,"snowflake","no escape");
  }
  else{
     QMessageBox::information(this,"snowflake","you pressed " + event->key());
  }
}
 
void MyWidget::keyReleaseEvent(QKeyEvent *event)
{

}


void MyWidget::EnableMouse()
{
  // you must set mouse tracking, it's off by default
  this->setMouseTracking(true);
}

void MyWidget::mouseMoveEvent(QMouseEvent *_event)
{
  if (this->rect().contains(_event->pos())) {
    // Mouse over Widget
    QMessageBox::information(this,"snowflake","That hurts! You sadist!!!!!!");
  }
  else {
    // Mouse out of Widget
  }
}

void MyWidget::paintEvent( QPaintEvent * )
{
  QPainter p( this );
  p.setPen( Qt::darkGray );
  p.drawRect( 2,4, 10,8 );
  p.setPen( Qt::lightGray );
  p.drawLine( 18,4, 14,14 );
}


// I don't like the default way the main window pops up, I want it bigger
class MyMainWindow : public QMainWindow {

public: 
  
  MyMainWindow();

};

MyMainWindow::MyMainWindow(){
  // my window asks how big the desktop is; this is a widget for QT
  resize(QDesktopWidget().availableGeometry(this).size() * 0.7);
  // I've set my window to 70 % of the desktop
}

int main(int argc, char **argv) {
  QApplication app(argc, argv);
  MyMainWindow window;

  MyWidget w;
  w.EnableMouse(); // don't forget this
  window.setCentralWidget(&w);
  window.show();

  return app.exec();
}
