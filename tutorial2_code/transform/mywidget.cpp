#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <fstream>
#include "mywidget.hpp"

 static const QPoint hourHand[3] ={
    QPoint(7,8),
    QPoint(-7,8),
    QPoint(0,-40)
   };
  
  static const QPoint minuetHand[3] ={
    QPoint(7,8),
    QPoint(-7,8),
    QPoint(0,-70)
   };

 // QColor hourColor(127,0,127);
 // QColor minuetColor(0,127,127,191); 
