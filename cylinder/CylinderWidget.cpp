#include <GL/glu.h>
#include <QGLWidget>
#include <cmath>
#include "CylinderWidget.h"

// Play with these parameters
static const int N        = 100; // This determines the number of faces of the cylinder
static const int n_div   =  1;  // This determines the fineness of the cylinder along its length


// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8 
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};

static materialStruct uglyMaterials = {
  { 1.0 , 0.0, 0.0, 1.0},
  { 0. ,  0.0,  0.0, 1.0},
  { 0.0, 0.0,  1.0, 1.0},
  100.0 
};

static const float PI = 3.1415926535;

// constructor
CylinderWidget::CylinderWidget(QWidget *parent)
  : QGLWidget(parent),
    _angle(0)
	{ // constructor       

	} // constructor

// called when OpenGL context is set up
void CylinderWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
	glOrtho(-4.,4.,-4.,4.,-4.,4);
 
	} // initializeGL()

void CylinderWidget::updateAngle(int i){
  _angle = i;
  this->repaint();
}

// called every time the widget is resized
void CylinderWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat light_pos[] = {2., -2.,-2., 1.};

	glEnable(GL_LIGHTING); // enable lighting in general
  glEnable(GL_LIGHT0);   // each light source must also be enabled


	materialStruct* p_front = &brassMaterials;
	
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
	
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);

	} // resizeGL()

void CylinderWidget::normalize(GLfloat* vector)
{
  GLfloat norm = sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
  vector[0] /= norm;
  vector[1] /= norm;
  vector[2] /= norm; 
}

void CylinderWidget::cylinder_by_polygon(){
  float x0, x1, y0, y1;
  for (int i = 0; i < N; i++){
    x0 = cos(2*i*PI/N);
    x1 = cos(2*(i+1)*PI/N);
    y0 = sin(2*i*PI/N);
    y1 = sin(2*(i+1)*PI/N);


    // CCW:
    // (x0, y0, -1) -> (x1, y1, -1) -> (x1, y1, 1) -> (x0, y0, -1)
    // The normal of such a plane is given by (x1 - x0, y1 - y1, 0) ^ (0, 0, 1) and point outwards
    // This is the vector ([y1 - x0], -[x1-x0], 0)

    GLfloat normal[3];
    normal[0]  = y1 - y0;
    normal[1]  = x0 - x1;
    normal[2]  = 0.;

    normalize(normal);

    glNormal3fv(normal);
    glBegin(GL_POLYGON);
    glVertex3f(x0,y0,-2.);
    glVertex3f(x1,y1,-2.);
    glVertex3f(x1,y1, 2.);
    glVertex3f(x0,y0, 2.);
    glEnd();
    
  }
}




void CylinderWidget::cylinder(){

  float x0, x1, y0, y1;

  float z_min = -2;
  float z_max =  2;

  float delta_z = (z_max - z_min)/n_div;
  
  for (int i = 0; i < N; i++){
    for(int i_z = 0; i_z < n_div; i_z++){
      x0 = cos(2*i*PI/N);
      x1 = cos(2*(i+1)*PI/N);
      y0 = sin(2*i*PI/N);
      y1 = sin(2*(i+1)*PI/N);

      float z = z_min + i_z*delta_z;
      glBegin(GL_POLYGON);
      glVertex3f(x0,y0,z);
      glNormal3f(x0,y0,0);
      glVertex3f(x1,y1,z);
      glNormal3f(x1,y1,0);
      glVertex3f(x1,y1,z+delta_z);
      glNormal3f(x1,y1,0);
      glVertex3f(x0,y0,z+delta_z);
      glNormal3f(x0,y0,0);
      glEnd();
    }
  }
}
	
// called every time the widget needs painting
void CylinderWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_NORMALIZE);
        glShadeModel(GL_FLAT);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
       	glEnable(GL_DEPTH_TEST); // 
	glLoadIdentity();
       	gluLookAt(2.,-2.,2., 0.0,0.0,0.0, 0.0,0.0,1.0);

	glRotatef((double)_angle,0.,0.,1.);

	this->cylinder();

	
	// flush to screen
	glFlush();	

	} // paintGL()
