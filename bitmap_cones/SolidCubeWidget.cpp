#include <GL/glu.h>
#include <QGLWidget>
#include "SolidCubeWidget.h"

// constructor
SolidCubeWidget::SolidCubeWidget(QWidget *parent)
	: QGLWidget(parent)
	{ // constructor

	  cone = gluNewQuadric();

	  // Create a checkerboard pattern
	  for ( int i = 0; i < 64; i++ ) {
	    for ( int j = 0; j < 64; j++ ) {
	      GLubyte c = (((i & 0x8) == 0) ^ ((j & 0x8)  == 0)) * 255;
	      check[i][j][0]  = c;
	      check[i][j][1]  = c;
	      check[i][j][2]  = c;
	    }
	  }

	} // constructor

// called when OpenGL context is set up
void SolidCubeWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	
 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, check);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
       	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
       	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
       	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
       
	} // initializeGL()

// called every time the widget is resized
void SolidCubeWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.,4.,-4.,4.,-4.,4.);

	} // resizeGL()


	
// called every time the widget needs painting
void SolidCubeWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
      	gluLookAt(0.,1.,1.0, 0.,0.,0.,0.,1.,0.);

       	glColor3f(1.,0.,0.);
            
	glTranslatef(-2.0,0.,0.);
      	glBegin(GL_QUADS);
       	glTexCoord2f(0.,0);
	glVertex3f(-1.,-1.,-1.);
      	glTexCoord2f(1.,0.);
	glVertex3f( 1.,-1.,-1.);
       	glTexCoord2f(1.,1.);
	glVertex3f( 1., 1.,-1.);
       	glTexCoord2f(0.,1.);
	glVertex3f(-1., 1.,-1.);
	glEnd();

       	glColor3f(0.,1.,0.);

      	glTranslatef(4.0,0.,0.);
      	glBegin(GL_QUADS);
      	glTexCoord2f(0.,0);
	glVertex3f(-1.,-1., 1.);
      	glTexCoord2f(1.,0.);
	glVertex3f( 1.,-0.5, 1.);
       	glTexCoord2f(1.,1.);
	glVertex3f( 1., 0.5, 1.);
       	glTexCoord2f(0.,1.);
	glVertex3f(-1., 1., 1.);
	glEnd();

	gluQuadricDrawStyle(cone, GLU_FILL);
	gluQuadricTexture(cone,GL_TRUE);
	gluCylinder(cone,1.0,0.,2.0,20,20);
	/*
       	glLoadIdentity();
       	glRasterPos2i(0,0);
       	glDrawPixels(64,64,GL_RGB, GL_UNSIGNED_BYTE, check);
	*/
	// flush to screen
	glFlush();	

	} // paintGL()
