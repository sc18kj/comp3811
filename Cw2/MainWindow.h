#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QTimer>
#include <QLabel>
#include "MainWidget.h"

class MainWindow: public QWidget{
 public:
    MainWindow(QWidget *parent);
    ~MainWindow();
    void ResetInterface();
    QBoxLayout *windowLayout;
    MainWidget *mainWidget;
    QSlider *xView;
    QSlider *yView;
    QSlider *zView;
    QSlider *craneAngle;

    QLabel *xLabel;
    QLabel *yLabel;
    QLabel *zLabel;
    QLabel *craneLabel;
    QTimer *timer;
};
#endif