#include <QApplication>
#include <QVBoxLayout>
#include <QtWidgets>
#include "MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
 	MainWindow *window = new MainWindow(NULL);
    window->resize(712, 612);
    window->show();
    window->setWindowTitle(QApplication::translate("renderer", "3D-renderer"));
	app.exec();
	delete window;
	return 0;
} 
