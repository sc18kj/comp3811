#ifndef _IMAGE_
#define _IMAGE_

#include <string>
#include <QImage>
#include <GL/glu.h>

class Image{
    public:
    Image(const std::string& fileName);
    ~Image();

    const GLubyte* imageField() const;

    //getters
    int getWidth(){return width;}
    int getHeight(){return height;}

    private:
    Image(const Image&);

    int width;
    int height;

    QImage* p_qimage;
    GLubyte* image;
};
#endif
