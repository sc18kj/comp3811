#include "Image.h"
#include <vector>
#include <iostream>
#include <cstdlib>

//the contructor will take the file path (given relitly) and will, load the image into GLubyets
Image::Image(const std::string& fileName){
    p_qimage= new QImage(QString(fileName.c_str()));
    width=p_qimage->width();
    height=p_qimage->height();
    image=new GLubyte[width*height*3]; //size matrix is width*height*3
    int nm= width*height;
    //for each pixled in the image dertmine the RGB vaule and load that into the matxi of GLubytes
    for(int i=0;i<nm;i++){
        std::div_t part=std::div((int)i,(int)width);
        QRgb colVal=p_qimage->pixel(width-part.rem-1,part.quot);
        image[3*nm-3*i-3]=qRed(colVal);
        image[3*nm-3*i-2]=qGreen(colVal);
        image[3*nm-3*i-1]=qBlue(colVal);
    }
}

//retunr thre Glubyte when reqietsd
const GLubyte* Image::imageField() const{
    return image;
}

Image::~Image(){
    delete image;
    delete p_qimage;
}