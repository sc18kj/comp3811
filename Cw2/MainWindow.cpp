#include "MainWindow.h"


MainWindow::MainWindow(QWidget *parent):QWidget(parent){

    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    mainWidget = new MainWidget(this);


    //crate all the slider labels
    xLabel=new QLabel("x axis view");
    yLabel=new QLabel("y axis view");
    zLabel=new QLabel("z axis view");
    craneLabel=new QLabel("crane arm roation");

    //rotate the camera as the sliders chage
    xView=new QSlider(Qt::Horizontal);
    connect(xView, SIGNAL(valueChanged(int)), mainWidget, SLOT(updateX(int)));
    yView=new QSlider(Qt::Horizontal);
    connect(yView, SIGNAL(valueChanged(int)), mainWidget, SLOT(updateY(int)));
    zView=new QSlider(Qt::Horizontal);
    connect(zView, SIGNAL(valueChanged(int)), mainWidget, SLOT(updateZ(int)));
    //update teh crane angle on slider chage
    craneAngle=new QSlider(Qt::Horizontal);
    connect(craneAngle, SIGNAL(valueChanged(int)), mainWidget, SLOT(updateCraneAngle(int)));

    //get rid of the amsive amout of white space between the lable and the slider
    xLabel->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Fixed);
    yLabel->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Fixed);
    zLabel->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Fixed);
    craneLabel->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Fixed);


    //add all the widgets to the layout
    windowLayout->addWidget(mainWidget);
    windowLayout->addWidget(xLabel);
    windowLayout->addWidget(xView);
    windowLayout->addWidget(yLabel);
    windowLayout->addWidget(yView);
    windowLayout->addWidget(zLabel);
    windowLayout->addWidget(zView);
    windowLayout->addWidget(craneLabel);
    windowLayout->addWidget(craneAngle);

    //crate the timer for updating when required
    timer = new QTimer(this);
	timer->start(20);
	connect(timer, SIGNAL(timeout()),  mainWidget, SLOT(updateAngle()));
}

MainWindow::~MainWindow(){
	delete windowLayout;
    delete mainWidget;
    delete xView;
    delete yView;
    delete zView;
}

void MainWindow::ResetInterface(){
    mainWidget->update();
	update();
}

