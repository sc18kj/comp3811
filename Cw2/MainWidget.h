#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <cmath>
#include "MainWidget.h"
#include "Image.h"

typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;
class MainWidget: public QGLWidget
	{

	Q_OBJECT

	public:
	MainWidget(QWidget *parent);
	~MainWidget();

	public slots:
	void updateX(int);
	void updateY(int);
	void updateZ(int);
	void updateCraneAngle(int);
	void updateAngle();
	void updateTime();
	void updateDirection();

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:
      void plane(float xs,float ys,float zs,const materialStruct* m);
      void cuboid(float xs,float ys,float zs,const materialStruct* m);
	  void TexturedCube(float xs, float ys, float zs,const materialStruct* m,int texture);
	  void tetrahedron(float size,const materialStruct* m);
	  void car();
	  void crane();
	  void billBoard(int texture);
	  void cylinder();
	  double ang;
	  double time;
	  double Direction;
	  Image i1;
	  Image i2;
	  Image i3;
	  Image i4;
	  QImage* p_qimage;
	};
#endif
