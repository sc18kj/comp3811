#include <QGLWidget>
#include <GL/glu.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "MainWidget.h"

int xVal=0;
int yVal=0;
int zVal=1;
int cAngle=0;

/*
  crate stuctures based on the martal used,
  in this case Reguler material refers to the matrial used for the floor,
*/

static materialStruct RegulerMaterial = {
  { 0.135, 0.1745, 0.0215, 1.0},
  { 0.2775, 0.2775, 0.2775, 1.0},
  { 0.773911, 0.773911, 0.773911, 1.0},
  27.8 
};

static materialStruct BuildingMaterial = {
  { 0.25, 0.25, 0.25, 1.0},
  { 0.25, 0.25, 0.25, 1.0},
  { 0.5, 0.5, 0.5, 1.0},
  5.0
};

static materialStruct CarMaterial = {
  { 0.0, 0.0, 0.0, 1.0},
  { 0.5, 0.0, 0.0, 1.0},
  { 0.7, 0.6, 0.6, 1.0},
  32.6
};

//crate an array of GLunits whitch will hold the ID for each texture 1-3
GLuint texID[4];


MainWidget::MainWidget(QWidget *parent):QGLWidget(parent),ang(0.0),i1("Textures/Marc_Dekamps.ppm"),i2("Textures/Mercator-projection.ppm"),i3("Textures/index.jpeg"),i4("Textures/288378.jpg"){    
}


MainWidget::~MainWidget(){
}

void MainWidget::initializeGL(){ 
	// set the widget background colourang
	glClearColor(0.3, 0.3, 0.3, 0.0);

  glEnable(GL_TEXTURE_2D); 
  glEnable(GL_DEPTH_TEST); 
  glEnable(GL_LIGHTING); // enable lighting in general
  glEnable(GL_LIGHT0);   // each light source must also be enabled

  //intalise the tetxures
  glGenTextures(4,texID);

  //bind each texture to a GLunit as shown by the arry then scale and load each of the images
  glBindTexture(GL_TEXTURE_2D,texID[0]);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,i1.getWidth(),i1.getHeight(),0,GL_RGB,GL_UNSIGNED_BYTE,i1.imageField());    
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

  glBindTexture(GL_TEXTURE_2D,texID[1]);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,i2.getWidth(),i2.getHeight(),0,GL_RGB,GL_UNSIGNED_BYTE,i2.imageField());   
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 

  glBindTexture(GL_TEXTURE_2D,texID[2]);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,i3.getWidth(),i3.getHeight(),0,GL_RGB,GL_UNSIGNED_BYTE,i3.imageField());   
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 

  glBindTexture(GL_TEXTURE_2D,texID[3]);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,i4.getWidth(),i4.getHeight(),0,GL_RGB,GL_UNSIGNED_BYTE,i4.imageField());   
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 
 
 
}


void MainWidget::resizeGL(int w, int h){
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);  

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10, 10, -10,10, -100.0, 100.0);


}

//called when ever the slider is moved for the camera view
void MainWidget::updateAngle(){
  ang += 1.0;
  this->repaint();
}	

//called when ever time needs updateing (in this cas every frame)
void MainWidget::updateTime(){
  time += 1.0;
  this->repaint();
}	

// caleld when ever the sldier for the crane arm is moved
void MainWidget::updateDirection(){
  Direction += 1.0;
  this->repaint();
}	


//crate a plane absed on the sizes and material you give it
void MainWidget::plane(float xs,float ys,float zs,const materialStruct* m){
  glMaterialfv(GL_FRONT, GL_AMBIENT,    m->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    m->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   m->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   m->shininess);

  glBegin(GL_POLYGON);
    glVertex3f(-xs, -ys,zs);
    glVertex3f( xs, -ys, zs); 
    glVertex3f( xs,  ys, zs);
    glVertex3f(-xs,  ys, zs);
  glEnd();
}


//update the angle of the crane 
void MainWidget::updateCraneAngle(int craneAngle){
  cAngle=craneAngle;
}

//update the cmaera view
void MainWidget::updateX(int x){
  xVal=x;
  this->repaint();
}

void MainWidget::updateY(int y){
  yVal=y;
  this->repaint();
}

void MainWidget::updateZ(int z){
  zVal=z;
  this->repaint();
}

//cretae a rotating tetxured cube whitch is essialy a billboard
void MainWidget::billBoard(int texture){
 glPushMatrix();
 glRotatef(ang, 0.,0.,1.);
 this->TexturedCube(5.0,1.0,5.0,&BuildingMaterial,texture);
 glPopMatrix();
}

//crate an untextured cubed based of the sized you provide
void MainWidget::cuboid(float xs, float ys, float zs,const materialStruct* m){


  glMaterialfv(GL_FRONT, GL_AMBIENT,    m->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    m->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   m->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   m->shininess);

  //top face
  glBegin(GL_POLYGON);
    glVertex3f(xs, ys, -zs);
    glVertex3f( -xs, ys, -zs);
    glVertex3f( -xs,  ys, zs);
    glVertex3f(xs,  ys, zs);
  glEnd();

  //bottom face
  glBegin(GL_POLYGON);
    glVertex3f(xs, -ys, zs); 
    glVertex3f( -xs, -ys, zs); 
    glVertex3f( -xs,  -ys, -zs);
    glVertex3f(xs,  -ys, -zs);
  glEnd();

  //front face
  glBegin(GL_POLYGON);
    glVertex3f(xs, ys, zs);
    glVertex3f( -xs, ys, zs); 
    glVertex3f( -xs,  -ys, zs);
    glVertex3f(xs,  -ys, zs);
  glEnd();

  //back face
  glBegin(GL_POLYGON);
    glVertex3f(xs, -ys, -zs); 
    glVertex3f( -xs, -ys, -zs); 
    glVertex3f( -xs,  ys, -zs);
    glVertex3f(xs,  ys, -zs);
  glEnd();

  //left face
  glBegin(GL_POLYGON);
    glVertex3f(-xs, ys, zs);
    glVertex3f(-xs, ys, -zs); 
    glVertex3f(-xs, -ys, -zs);
    glVertex3f(-xs, -ys, zs);
  glEnd();

  //right face
  glBegin(GL_POLYGON);
    glVertex3f(xs, ys, -zs); 
    glVertex3f(xs, ys, zs); 
    glVertex3f(xs, -ys, zs);
    glVertex3f(xs, -ys, -zs);
  glEnd();
}

//simaler to the cuboid class only now will textured the cubes using GLubytes form the Texured ID provided so it can chose the correct texure
void MainWidget::TexturedCube(float xs, float ys, float zs,const materialStruct* m,int texture){

  glBindTexture(GL_TEXTURE_2D,texID[texture]);

  glMaterialfv(GL_FRONT, GL_AMBIENT,    m->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    m->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   m->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   m->shininess);

  //top face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(xs, ys, -zs);
    glTexCoord2f(1.0, 1.0);
    glVertex3f( -xs, ys, -zs);
    glTexCoord2f(1.0, 0.0);
    glVertex3f( -xs,  ys, zs);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(xs,  ys, zs);
  glEnd();

  //bottom face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(xs, -ys, zs); 
    glTexCoord2f(1.0, 0.0); 
    glVertex3f( -xs, -ys, zs); 
    glTexCoord2f(1.0, 1.0);
    glVertex3f( -xs,  -ys, -zs);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(xs,  -ys, -zs);
  glEnd();

  //front face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(xs, ys, zs); 
    glTexCoord2f(1.0, 1.0); 
    glVertex3f( -xs, ys, zs); 
    glTexCoord2f(1.0, 0.0);
    glVertex3f( -xs,  -ys, zs);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(xs,  -ys, zs);
  glEnd();

  //back face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(xs, -ys, -zs);
    glTexCoord2f(1.0, 0.0);  
    glVertex3f( -xs, -ys, -zs);   
    glTexCoord2f(1.0, 1.0);
    glVertex3f( -xs,  ys, -zs);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(xs,  ys, -zs);
  glEnd();

  //left face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-xs, ys, zs);
    glTexCoord2f(1.0, 0.0); 
    glVertex3f(-xs, ys, -zs); 
    glTexCoord2f(1.0, 1.0);
    glVertex3f(-xs, -ys, -zs);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-xs, -ys, zs);
  glEnd();

  //right face
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(xs, ys, -zs); 
    glTexCoord2f(1.0, 0.0);
    glVertex3f(xs, ys, zs); 
    glTexCoord2f(1.0, 1.0);
    glVertex3f(xs, -ys, zs);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(xs, -ys, -zs);
  glEnd();

  glBindTexture(GL_TEXTURE_2D,0);
}


//create a tehrathedron , no rela reason other than its required
void MainWidget::tetrahedron(float size,const materialStruct* m){

  glMaterialfv(GL_FRONT, GL_AMBIENT,    m->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    m->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   m->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   m->shininess);

  glPushMatrix();
  glBegin(GL_TRIANGLES);
  //front face
  glVertex3f(0.0f,size,0.0f);
  glVertex3f(-size,-size,0.0f);
  glVertex3f(size,-size,0.0f);

  //right face
  glVertex3f(size,-size,0.0f);
  glVertex3f(0.0f,size,0.0f);
  glVertex3f(0.0f,-size,-size);

  //left face
  glVertex3f(-size,-size,0.0f);
  glVertex3f(0.0f,size,0.0f);
  glVertex3f(0.0f,-size,-size);

  //bottom face
  glVertex3f(-size,-size,0.0f);
  glVertex3f(size,-size,0.0f);
  glVertex3f(0.0f,-size,-size);

  glEnd();


  glPopMatrix();

}

//crate a car from a serioes of objects whitch in clue a cylinder, and a few cubes
void MainWidget::car(){
 glPushMatrix();
  this->cuboid(0.75,1.5,0.75,&CarMaterial);
  glTranslatef(0.0,0.0,1.0);
  this->cuboid(0.75,0.5,0.75,&CarMaterial);

  glTranslatef(0.2,-0.5,-1.0);
  glRotatef(90.0,0.0,1.0,0.0);
  glScalef(0.75,0.75,0.75);
  this->cylinder();
  glTranslatef(0.0,2.0,0.0);
  this->cylinder();

 glPopMatrix();
}

//crate a crnae that is made of cubes and cylinders as shown in the turotials
void MainWidget::crane(){
  glPushMatrix();
  this->cylinder();
  glTranslatef(0.0,0.0,2.0);
  this->cuboid(1.0,1.0,1.0,&RegulerMaterial);
  glPushMatrix();
  glRotatef(cAngle,1.0,0.0,0.0);
  glTranslatef(0.0,0.0,3.0);
  this->cylinder();
  glRotatef(45.0,1.0,0.0,0.0);
  glTranslatef(0.0,0.0,2.5);
  this->cylinder();

  glPopMatrix();
  glPopMatrix();
}


//crate a culinder, this will alawasy be a fixed size and matrials 
void MainWidget::cylinder(){
  float x0, x1, y0, y1;
  float z_min = -2;
  float z_max =  2;
  float delta_z = (z_max - z_min)/1;
  for (int i = 0; i < 10; i++){
    for(int i_z = 0; i_z < 1; i_z++){
      x0 = cos(2*i*M_PI/10);
      x1 = cos(2*(i+1)*M_PI/10);
      y0 = sin(2*i*M_PI/10);
      y1 = sin(2*(i+1)*M_PI/10);

      float z = z_min + i_z*delta_z;
      glBegin(GL_POLYGON);
      glVertex3f(x0,y0,z);
      glNormal3f(x0,y0,0);
      glVertex3f(x1,y1,z);
      glNormal3f(x1,y1,0);
      glVertex3f(x1,y1,z+delta_z);
      glNormal3f(x1,y1,0);
      glVertex3f(x0,y0,z+delta_z);
      glNormal3f(x0,y0,0);
      glEnd();
    }
  }
}

// called every time the widget needs painting
void MainWidget::paintGL(){ 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_NORMALIZE);
  glShadeModel(GL_FLAT);

	// You must set the matrix mode to model view directly before enabling the depth test
  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST); // 
	glLoadIdentity();
  gluLookAt(2.,-2.,2., xVal,yVal,zVal,xVal,yVal,zVal); //camer veiwing angle will be upstaed as the slider dose

  // this is where i create all the objets of the secne most of it is cubes or cyliders
  this->TexturedCube(0.75,0.75,1.5,&BuildingMaterial,2);

  glPushMatrix();
  for(int i=0;i<4;i++){
    this->TexturedCube(0.75,0.75,2.5,&BuildingMaterial,2);
    glTranslatef(-3.0,0.0,0.0);
  }

  glTranslatef(15.0,-4.0,0.0);
  this->TexturedCube(1.0,0.75,4.5,&BuildingMaterial,2);

  glTranslatef(2.0,7.0,0.0);
  this->TexturedCube(1.0,0.75,4.5,&BuildingMaterial,2);

  glTranslatef(0.0,-15.0,0.0);
  this->cuboid(1.0,1.5,0.5,&BuildingMaterial);
  this->cylinder();
  glTranslatef(0.0,0.0,3.0);
  this->tetrahedron(1.0,&BuildingMaterial);
  
  glPushMatrix();
  glTranslatef(10.0,-1.0,0.0);
  glScalef(0.5,0.5,0.5);
  this->car();
  glPopMatrix();

  glPushMatrix();
  glTranslatef(-3.0,5.0,0.0);
  this->crane();
  glPopMatrix();

  glScalef(0.5,0.5,0.5);
  glTranslatef(10.0,-1.0,12.0);
  this->billBoard(0);

  glTranslatef(-15.0,-4.0,-10.0);
  this->billBoard(1);

  glPopMatrix();
   
  glPushMatrix();
  glTranslatef(-3.0,-3.0,-3.0);
  this->plane(10,10,0,&RegulerMaterial);   
  glPopMatrix();

  // flush to screen 
	glFlush();	
}
