#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <fstream>
#include "pixelwidget.hpp"

void PixelWidget::DefinePixelValues(){ //add pixels here; write methods like this for the assignment
  SetPixel(1,10,RGBVal(0,50,205));
}


// -----------------Most code below can remain untouched -------------------------
// ------but look at where DefinePixelValues is called in paintEvent--------------

PixelWidget::PixelWidget(unsigned int n_vertical, unsigned int n_horizontal):
  _n_vertical   (n_vertical),
  _n_horizontal (n_horizontal),
  _vec_rects(0)
{
  // all pixels are initialized to black
     for (unsigned int i_col = 0; i_col < n_vertical; i_col++)
       _vec_rects.push_back(std::vector<RGBVal>(n_horizontal));
}



void PixelWidget::SetPixel(unsigned int i_column, unsigned int i_row, const RGBVal& rgb)
{

  // if the pixel exists, set it
  if (i_column < _n_vertical && i_row < _n_horizontal)
    _vec_rects[i_column][i_row] = rgb;
}


void PixelWidget::paintEvent( QPaintEvent * )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, false );

  // set window/viewport so that the size fits the screen, within reason
  p.setWindow(QRect(-1.,-1.,_n_vertical+2,_n_horizontal+2));
  int side = qMin(width(), height());  
  p.setViewport(0, 0, side, side);

  // black thin boundary around the cells
  QPen pen( Qt::black );
  pen.setWidth(0.);


  // here the pixel values defined by the user are set in the pixel array
  DefinePixelValues();
  DrawLine(1.0,60.0,10.0,60.0,RGBVal(255,255,255),RGBVal(255,255,255));
  DrawLine(1.0,30.0,50.0,50.0,RGBVal(255,0,0),RGBVal(0,255,0));
  createTriangle(1.0,1.0,35.0,1.0,35.0,40.0,RGBVal(255,0,0),RGBVal(0,255,0),RGBVal(0,0,255));
  for(int i=0;i<70;i++){
    for(int j=0;j<70;j++){
      IsInside(1.0,1.0,35.0,1.0,35.0,40.0,i,j);
    }
  }
  createPPMImage();
  for (unsigned int i_column = 0 ; i_column < _n_vertical; i_column++){
    for(unsigned int i_row = 0; i_row < _n_horizontal; i_row++){
      QRect rect(i_column,i_row,1,1);
      QColor c = QColor(_vec_rects[i_column][i_row]._red, _vec_rects[i_column][i_row]._green, _vec_rects[i_column][i_row]._blue);
    
      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
      p.setPen(pen);
      p.drawRect(rect);
    }
  }
}

void PixelWidget::DrawLine(float xStart,float yStart,float xEnd,float yEnd,RGBVal CStart,RGBVal CEnd){

  float x=xStart-xEnd; // range between xStart and xEnd
  float y=yStart-yEnd; // range between yStart and yEnd
  float gradeint; //coefficdnt of the line
  
  //calue late the number of pixles between point xstart,ystrart and xend,yend 
  int numSteps=sqrt(pow((xEnd-xStart),2)+pow((yEnd-yStart),2));
  int redInc=(CStart._red-CEnd._red)/numSteps; //get the distace in steps for colour of red e.g [255,0,0] and [0,255,0]
  int greenInc=(CStart._green-CEnd._green)/numSteps;
  int blueInc=(CStart._blue-CEnd._blue)/numSteps;
  int red,green,blue;
  //set up how much red needs to increace or decreaer per step
  if(CStart._red<CEnd._red){
    redInc=(CEnd._red-CStart._red)/numSteps;
  }
  if(CStart._green<CEnd._green){
     greenInc=(CEnd._green-CStart._green)/numSteps;
  }
  if(CStart._blue<CEnd._blue){
     blueInc=(CEnd._blue-CStart._blue)/numSteps;
  }

  //if the vector x is greater than the vector y then draw a line form x to y diaonaly
  if(abs(x)>abs(y)){
    gradeint=y/x; 
    if(xStart<=xEnd){
     for(x=xStart;x<=xEnd;x+=0.01f){ //incece by 0.01 to ensure thet every pixle is passed though
      y=yStart+((x-xStart)*gradeint); // liner coefficet of the line
      //determine wather or not to increce or decrenst the colur value based on if the increcing value is negative or not
      if(CStart._red<CEnd._red){
        red=CStart._red+(redInc*x);
      }
      else{
        red=CStart._red-(redInc*x);
      }
      if(CStart._green<CEnd._green){
         green=CStart._green+(greenInc*x);
      }
      else{
        green=CStart._green-(greenInc*x);
        if(green<0){
          green=0;
        }
      }
      if(CStart._blue<CEnd._blue){
         blue=CStart._blue+(blueInc*x);
      }
      else{
         blue=CStart._blue-(blueInc*x);
      }
      SetPixel(x,y,RGBVal(red,green,blue));
     }
    }
    else{
     for(x=xEnd;x<xStart;x+=0.01f){ //incece by 0.01 to ensure thet every pixle is passed though
      y=yStart+((x-xStart)*gradeint);
      if(CStart._red<CEnd._red){
         red=CStart._red+(redInc*x);
      }
      else{
        red=CStart._red-(redInc*x);
      }
      if(CStart._green<CEnd._green){
         green=CStart._green+(greenInc*x);
      }
      else{
        green=CStart._green-(greenInc*x);
      }
      if(CStart._blue<CEnd._blue){
         blue=CStart._blue+(blueInc*x);
      }
      else{
        blue=CStart._blue-(blueInc*x);
      }
      SetPixel(x,y,RGBVal(red,green,blue));
     }
    }
  
  }
  //else eihter draw a horizontal line or a line form y to x
  if(fabs(x)<=fabs(y)){
    gradeint=x/y;
    if(yStart<yEnd){
     for(y=yStart;y<=yEnd;y+=0.01f){
      x=xStart+((y-yStart)*gradeint);
      if(CStart._red<CEnd._red){
         red=CStart._red+(redInc*y);
      }
      else{
        red=CStart._red-(redInc*y);
      }
      if(CStart._green<CEnd._green){
        green=CStart._green+(greenInc*y);
      }
      else{
        green=CStart._green-(greenInc*y);
      }
      if(CStart._blue<CEnd._blue){
         blue=CStart._blue+(blueInc*y);
      }
      else{
         blue=CStart._blue-(blueInc*y);
      }
      SetPixel(x,y,RGBVal(red,green,blue));
     }
    }
    else{
     for(y=yEnd;y<yStart;y+=0.01f){
      x=xStart+((y-yStart)*gradeint);
      if(CStart._red<CEnd._red){
         red=CStart._red+(redInc*y);
      }
      else{
        red=CStart._red-(redInc*y);
      }
      if(CStart._green<CEnd._green){
         green=CStart._green+(greenInc*y);
      }
      else{
        green=CStart._green-(greenInc*y);
      }
      if(CStart._blue<CEnd._blue){
         blue=CStart._blue+(blueInc*y);
      }
      else{
         blue=CStart._blue-(blueInc*y);
      }
      SetPixel(x,y,RGBVal(red,green,blue));
     }
    }
  }

}

/*
 draw lines form each vertex on the egdes of the trangle and intupolate the colours 
*/

void PixelWidget::createTriangle(float x1,float y1,float x2,float y2,float x3,float y3,RGBVal Point1,RGBVal Point2,RGBVal Point3){
  float alpha,beta,gamma,red,green,blue;
  //draw the trigle edges
  DrawLine(x1,y1,x2,y2,Point1,Point2);
  DrawLine(x2,y2,x3,y3,Point2,Point3);
  DrawLine(x3,y3,x1,y1,Point1,Point3);
  //loop though the drid and assieng each point an alpha, beta and gamma cordinate based on the trigle cordintes
  for(int i=0;i<70;i++){
    for(int j=0;j<70;j++){
      alpha=((y2-y3)*(i-x3)+(x3-x2)*(j-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
      beta=((y3-y1)*(i-x3)+(x1-x3)*(j-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
      gamma=1-alpha-beta;
      //if alpha,beta and gamma are above 0 and bellow 1 it is in the triangle so set the new red green and blue vaies to inturplate
      if(alpha>=0 && beta>=0 && gamma>=0 && alpha<=1 && beta<=1&& gamma<=1 && alpha+beta<=1){
        red=alpha*Point1._red + beta*Point2._red + gamma*Point3._red;
        green=alpha*Point1._green + beta*Point2._green + gamma*Point3._green;
        blue=alpha*Point1._blue + beta*Point2._blue + gamma*Point3._blue;
        SetPixel(i,j,RGBVal(red,green,blue));
      }
    }
  }

}

void PixelWidget::IsInside(float x1,float y1,float x2,float y2,float x3,float y3,float px,float py){
  //mkae the alpha btea and gamma agian based on tigle cordinates 
  float alpha=((y2-y3)*(px-x3)+(x3-x2)*(py-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
  float beta=((y3-y1)*(px-x3)+(x1-x3)*(py-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
  float gamma=1-alpha-beta;
  //caluet the vetor distce between each point
  float d1=(px-x2)*(y1-y2)-(x1-x2)*(py-y2); //distace between x1,y1 and x2,y2
  float d2=(px-x3)*(y2-y3)-(x2-x3)*(py-y3); // distace between x2,y2 and x3,y3
  float d3=(px-x1)*(y3-y1)-(x3-x1)*(py-y1); // distace between x1,y1 and x3,y3

  std::ofstream file("coordinates.txt",std::ofstream::app);
  file<<"testing for Cordinate "<<px<<","<<py<<" is in the tringle";
  //if the disce is gretaer than 0 it must be in the tringle
  if(d1>=0 && d2>=0 && d3>=0){
    file<<" alpha="<<alpha<<" beta="<< beta<<" gamma="<<gamma<<" True\n";
  }
  else{
    file<<" alpha="<<alpha<<" beta="<< beta<<" gamma="<<gamma<<" False\n";
  }

  file.close();

}

void PixelWidget::createPPMImage(){
  float alpha,beta,gamma;
  int red,green,blue;
  std::ofstream file("PPMimage.ppm");
  file<<"P6\n"<<70<<" "<<70<<" 255\n";
  for(int i=0;i<70;i++){
    for(int j=0;j<70;j++){
      alpha=((1.0-50.0)*(i-50.0)+(50.0-50.0)*(j-50.0))/((1.0-50.0)*(1.0-50.0)+(50.0-50.0)*(1.0-50.0));
      beta=((50.0-1.0)*(i-50.0)+(1.0-50.0)*(j-50.0))/((1.0-50.0)*(1.0-50.0)+(50.0-50.0)*(1.0-50.0));
      gamma = 1-alpha-beta;
      if(alpha>=0 && beta>=0 && gamma>=0 && alpha<=1 && beta<=1&& gamma<=1 && alpha+beta<=1){
        red=alpha*255;
        green=beta*255;
        blue=gamma*255;
        file<<uint8_t(red)<<uint8_t(green)<<uint8_t(blue);
      }
      else{
        file<<uint8_t(0)<<uint8_t(0)<<uint8_t(0);
      }
    }
  }
  file.close();
}

